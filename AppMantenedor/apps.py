from django.apps import AppConfig


class AppmantenedorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AppMantenedor'
